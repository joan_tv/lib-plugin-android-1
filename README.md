# plugin-lib-android
[![build](https://bitbucket-badges.atlassian.io/badge/npaw/lib-plugin-android.svg)](https://bitbucket.org/npaw/lib-plugin-android/)
[![codecov](https://codecov.io/bb/npaw/lib-plugin-android/branch/master/graph/badge.svg)](https://codecov.io/bb/npaw/lib-plugin-android)

## Documentation, Installation & Usage
Please refer to [Developer Portal](http://developer.nicepeopleatwork.com).

## I need help!
If you find a bug, have a suggestion or need assistance send an E-mail to <support@nicepeopleatwork.com>
