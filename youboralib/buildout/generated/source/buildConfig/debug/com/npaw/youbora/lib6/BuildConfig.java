/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.npaw.youbora.lib6;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.npaw.youbora.lib6";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 6001004;
  public static final String VERSION_NAME = "6.1.4";
}
