package com.npaw.youbora.lib6;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides a set of convenience methods to ease the logging.
 * @author      Nice People at Work
 * @since       6.0
 */
public class YouboraLog {

    /**
     * List of {@link YouboraLogger} instances.
     *
     * You can register to Youbora logs by calling the {@link #addLogger(YouboraLogger)} method.
     *
     * This can be useful for example to dump the log messages to a file.
     * @see YouboraLogger
     */
    private static List<YouboraLogger> loggers;

    /**
     * Enum for log levels
     */
    public enum Level {

        /** No console outputs */
        SILENT(6),
        /** Console will show errors */
        ERROR(5),
        /** Console will show warnings */
        WARNING(4),
        /** Console will show notices (ie: lifecycle logs) */
        NOTICE(3),
        /** Console will show debug messages (ie: player events) */
        DEBUG(2),
        /** Console will show verbose messages (ie: Http Requests) */
        VERBOSE(1);

        private int level;

        Level(int level) {
            this.level = level;
        }

        /**
         * Returns an int representation of the current log level.
         * @return int representation of the current log level.
         */
        public int getLevel() {
            return level;
        }

        /**
         * Returns true if the current log level is less or equal than the param.
         * This is useful to avoid calling logging methods when they're not going to be printed, for
         * instance when it's expensive to generate the string to log.
         * @param lev The {@link Level} to check agains.
         * @return true if the current {@link #level} is less than or equal to {@code lev}.
         */
        public boolean isAtLeast(Level lev) {
            return lev.getLevel() <= this.level;
        }
    }

    /** Tag for logging with the {@link android.util.Log} class */
    private static final String TAG = "Youbora";

    /**
     * Only logs of this imporance or higher will be shown.
     * @see {@link Level}
     */
    private static Level currentLogLevel = Level.ERROR;

    /**
     * Prints a message using Android's {@link YouboraLog} class. The message is only logged if the
     * {@link #currentLogLevel} is lower or equal than the logLevel. However, all the
     * {@link YouboraLogger} added with {@link #addLogger(YouboraLogger)} will be called
     * regardless of the log level.
     * @param logLevel The log level at which to log the message
     * @param message The message to log
     */
    public static void reportLogMessage(Level logLevel, String message) {

        if (loggers != null) {
            for (YouboraLogger logger : loggers) {
                logger.logYouboraMessage(message, logLevel);
            }
        }

        if (currentLogLevel.getLevel() <= logLevel.getLevel()) {
            switch (logLevel) {
                case ERROR:
                    android.util.Log.e(TAG, message);
                    break;
                case WARNING:
                    android.util.Log.w(TAG, message);
                    break;
                case NOTICE:
                    android.util.Log.i(TAG, message);
                    break;
                case DEBUG:
                    android.util.Log.d(TAG, message);
                    break;
                case VERBOSE:
                    android.util.Log.v(TAG, message);
                    break;
                default:
            }
        }
    }

    /**
     * Same as calling {@link #reportLogMessage(Level, String)} with {@link Level#ERROR} level.
     * @param message the message to log
     */
    public static void error(String message) {
        reportLogMessage(Level.ERROR, message);
    }

    /**
     * Logs the exception and prints its stack trace if the {@link #currentLogLevel} is high enough.
     * @param exception The exception to log
     */
    public static void error(Exception exception) {
        if (currentLogLevel.getLevel() <= Level.ERROR.getLevel() ||
                (loggers != null && loggers.size() > 0)) {

            // Stack trace to string
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            exception.printStackTrace(pw);

            reportLogMessage(Level.ERROR, sw.toString());
        }

    }

    /**
     * Same as calling {@link #reportLogMessage(Level, String)} with {@link Level#WARNING} level.
     * @param message the message to log
     */
    public static void warn(String message) {
        reportLogMessage(Level.WARNING, message);
    }

    /**
     * Same as calling {@link #reportLogMessage(Level, String)} with {@link Level#NOTICE} level.
     * @param message the message to log
     */
    public static void notice(String message) {
        reportLogMessage(Level.NOTICE, message);
    }

    /**
     * Same as calling {@link #reportLogMessage(Level, String)} with {@link Level#DEBUG} level.
     * @param message the message to log
     */
    public static void debug(String message) {
        reportLogMessage(Level.DEBUG, message);
    }

    /**
     * Same as calling {@link #reportLogMessage(Level, String)} with {@link Level#VERBOSE} level.
     * @param message the message to log
     */
    public static void requestLog(String message) {
        reportLogMessage(Level.VERBOSE, message);
    }

    /**
     * Getter for current log level
     * @return the current log level
     */
    public static Level debugLevel() {
        return currentLogLevel;
    }

    /**
     * Sets the log level. Any YouboraLog message with level lower o equal to {@link #currentLogLevel} will
     * be printed.
     * @param debugLevel the log level to set
     */
    public static void setDebugLevel(Level debugLevel) {
        currentLogLevel = debugLevel;
    }

    /**
     * Add an external logger to receive the log messages.
     * @param logger YouboraLogger instance
     */
    public static void addLogger(YouboraLogger logger) {
        if (logger != null) {
            if (loggers == null) {
                loggers = new ArrayList<YouboraLogger>(1);
            }
            loggers.add(logger);
        }
    }

    public static boolean removeLogger(YouboraLogger logger) {
        if (logger == null || loggers == null) {
            return false;
        } else {
            return loggers.remove(logger);
        }
    }

    /**
     * Interface that receives log messages as reported by {@link YouboraLog} class.
     * @author      Nice People at Work
     * @since       6.0
     */
    public interface YouboraLogger {
        /**
         * This will be invoked whenever a log message is created
         * @param message The log message
         * @param logLevel The log level of the message
         */
        void logYouboraMessage(String message, Level logLevel);
    }
}