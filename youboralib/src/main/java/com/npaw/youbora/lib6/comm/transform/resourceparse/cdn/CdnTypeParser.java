package com.npaw.youbora.lib6.comm.transform.resourceparse.cdn;


/**
 * Interface to delegate the logic of mapping the value returned in a CDN response header to
 * the YOUBORA expected values.
 *
 * @author      Nice People at Work
 * @since       6.0
 */
public interface CdnTypeParser {

    /**
     * Possible YOUBORA Cdn Types.
     */
    enum Type {

        Unknown(0),
        Hit(1),
        Miss(2);

        private int value;

        /**
         * Enum constructor
         * @param value int value
         */
        Type(int value) {
            this.value = value;
        }

        /**
         * Integer representation of the Type. This value is the one that should be sent to YOUBORA
         * @return the value as an integer
         */
        public int getValue() {
            return value;
        }
    }

    /**
     * This callback will be invoked whenever the CdnType is found in a CDN response header as
     * specified by a {@link CdnParsableResponseHeader}.
     * The type param will contain whatever the capturing group in the {@link CdnParsableResponseHeader#regexPattern}
     * has marked as Type in {@link CdnParsableResponseHeader#element}.
     * @param type the string to be parsed matching a capturing group
     * @return the parsed {@link Type} based on the param content.
     */
    Type parseCdnType(String type);
}