package com.npaw.youbora.lib6.adapter;

import com.npaw.youbora.lib6.Chrono;

/**
 * This class contains all the {@link Chrono}s related to view status.
 * Chronos measure time lapses between events.
 * ie: between start and join, between seek-begin and seek-end, etc.
 * Each plugin will have an instance of this class.
 * @author      Nice People at Work
 * @since       6.0
 */
public class PlaybackChronos {

    /** Chrono between start and joinTime. */
    public Chrono join;

    /** Chrono between seek-begin and seek-end. */
    public Chrono seek;

    /** Chrono between pause and resume. */
    public Chrono pause;

    /** Chrono between buffer-begin and buffer-end. */
    public Chrono buffer;

    /** Chrono for the totality of the view. */
    public Chrono total;

    /** Chrono for the ad init time. */
    public Chrono adInit;

    /**
     * Constructor
     */
    public PlaybackChronos() {
        reset();
    }

    /**
     * Reset flag values
     */
    public void reset() {
        join = new Chrono();
        seek = new Chrono();
        pause = new Chrono();
        buffer = new Chrono();
        total = new Chrono();

        adInit = new Chrono();
    }

}
