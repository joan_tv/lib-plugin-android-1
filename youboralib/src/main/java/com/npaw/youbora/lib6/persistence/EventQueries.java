package com.npaw.youbora.lib6.persistence;

/**
 * Created by Enrique on 25/01/2018.
 */

public class EventQueries {
    public static final String EVENT_CREATE_TABLE = "CREATE TABLE `"+
            OfflineContract.OfflineEntry.TABLE_NAME+"` (`"+
            OfflineContract.OfflineEntry.COLUMN_NAME_UID+"` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `"+
            OfflineContract.OfflineEntry.COLUMN_NAME_JSON_EVENTS+"` TEXT, `"+
            OfflineContract.OfflineEntry.COLUMN_NAME_DATE_UPDATE+"` INTEGER, `"+
            OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID+"` INTEGER NOT NULL)";

    public static final String EVENT_INSERT = "INSERT INTO "+
            OfflineContract.OfflineEntry.TABLE_NAME+
            "("+OfflineContract.OfflineEntry.COLUMN_NAME_JSON_EVENTS+
            ","+OfflineContract.OfflineEntry.COLUMN_NAME_DATE_UPDATE+
            ","+OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID+
            ") values (?,?,?)";

    public static final String EVENT_GET_ALL = "SELECT * FROM "+
            OfflineContract.OfflineEntry.TABLE_NAME+"";
    public static final String EVENT_GET_LAST_ID = "Select "+
            OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID+
            " from "+OfflineContract.OfflineEntry.TABLE_NAME+
            " ORDER BY "+
            OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID+" DESC LIMIT 1";

    public static final String EVENT_GET_BY_OFFLINE_ID = "Select * from "+
            OfflineContract.OfflineEntry.TABLE_NAME+" where "+
            OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID+" = %s";

    public static final String EVENT_GET_FIRST_ID = "Select "+
            OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID+
            " from "+OfflineContract.OfflineEntry.TABLE_NAME+
            " ORDER BY "+OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID+
            " ASC LIMIT 1";

    public static final String EVENT_DELETE_EVENTS_BY_OFFLINE_ID= "Delete from "+
            OfflineContract.OfflineEntry.TABLE_NAME+" where "+
            OfflineContract.OfflineEntry.COLUMN_NAME_OFFLINE_ID+" = %s";
}
