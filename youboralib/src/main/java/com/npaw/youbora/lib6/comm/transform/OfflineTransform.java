package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.persistence.EventDataSource;
import com.npaw.youbora.lib6.persistence.entity.Event;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by Enrique on 20/07/2017.
 */

public class OfflineTransform extends Transform {

    private EventDataSource dataSource;

    public OfflineTransform() {
        sendRequest = false;
        isBusy = false;
    }

    @Override
    public void parse(Request request) {
        if (request != null && request.getParams() != null) {
            Map<String,Object> messageData = request.getParams();
            saveEvent(messageData, request.getService().substring(1));
        }
    }

    @Override
    public boolean hasToSend(Request request) {
        return false;
    }

    @Override
    public int getState() {
        return Transform.STATE_OFFLINE;
    }

    /**
     * Appends the new offline event to the existing ones
     *
     * @param service Event service
     */
    private void saveEvent(final Map<String,Object> messageData, final String service){
        dataSource = new EventDataSource();
        dataSource.initDatabase();

        if(service.equals(Constants.SERVICE_INIT.substring(1))){
            return;
        }

        messageData.put("request",service);
        messageData.put("unixtime",System.currentTimeMillis());

        dataSource.getLastId(new EventDataSource.QuerySuccessListener() {
            @Override
            public void onQueryResolved(Object queryResult) {
                final int finalOffline_id = (Integer) queryResult;
                dataSource.getAllEvents(new EventDataSource.QuerySuccessListener() {
                    @Override
                    public void onQueryResolved(Object queryResult) {
                        final int eventsCount = (int) ((List<Event>) queryResult).size();
                        int offline_id = finalOffline_id;
                        if(eventsCount != 0 && service.equals(Constants.SERVICE_START.substring(1))){
                            offline_id++;
                        }
                        messageData.put("code",String.format("[VIEW_CODE]_%s",String.valueOf(offline_id)));
                        JSONObject json = new JSONObject(messageData);

                        YouboraLog.debug(String.format("Saving offline event %s: %s",service ,json.toString()));
                        dataSource.insertNewElement(new Event(json.toString(),System.currentTimeMillis(), offline_id),null);
                    }
                });
            }
        });
    }
}
