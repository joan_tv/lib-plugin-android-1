package com.npaw.youbora.lib6.persistence;

import android.content.Context;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.persistence.dao.EventDAOImpl;
import com.npaw.youbora.lib6.persistence.entity.Event;
import com.npaw.youbora.lib6.persistence.helper.EventDbHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Enrique on 28/12/2017.
 */

public class EventDataSource {

    private EventDbHelper databaseInstance;
    private Context context;

    private List<EventDataSource.QuerySuccessListener> successListenerList = new ArrayList<>(1);
    private List<EventDataSource.QueryUnsuccessListener> errorListenerList = new ArrayList<>(1);

    public EventDataSource() { }

    public EventDataSource(Context context) {
        this.context = context;
    }

    public void initDatabase() {
        if(AppDatabaseSingleton.getInstance() != null){
            databaseInstance = AppDatabaseSingleton.getInstance();
            return;
        }
        if(context != null && databaseInstance == null){
            AppDatabaseSingleton.init(context);
            databaseInstance = AppDatabaseSingleton.getInstance();
            return;
        }
        if (context == null && databaseInstance == null) {
            YouboraLog.error("Null context or AppDatabaseSingleton not initiated");
        }
    }

    public void initDatabase(Context context) {
        AppDatabaseSingleton.init(context);
        databaseInstance = AppDatabaseSingleton.getInstance();
    }

    public void insertNewElement(Event event,QuerySuccessListener listener){
        if(databaseInstance != null) {
            final QuerySuccessListener finalListener = listener;
            final Event finalEvent = event;
            new Thread(new Runnable() {
                public void run() {
                    long returnedValue = EventDAOImpl.getInstance().insertNewEvent(finalEvent);
                    if (finalListener != null) {
                        finalListener.onQueryResolved(returnedValue);
                    }
                }
            }).start();
        }
    }

    public void getAllEvents(QuerySuccessListener listener){
        if(databaseInstance != null) {
            final QuerySuccessListener finalListener = listener;
            new Thread(new Runnable() {
                public void run() {
                    List<Event> returnedValue = EventDAOImpl.getInstance().getAll();
                    if (finalListener != null) {
                        finalListener.onQueryResolved(returnedValue);
                    }
                }
            }).start();
        }
    }

    public void getLastId(QuerySuccessListener listener){
        if(databaseInstance != null) {
            final QuerySuccessListener finalListener = listener;
            new Thread(new Runnable() {
                public void run() {
                    int returnedValue = EventDAOImpl.getInstance().getLastId();
                    if (finalListener != null) {
                        finalListener.onQueryResolved(returnedValue);
                    }
                }
            }).start();
        }
    }

    public void getByOfflineId(final int offlineId, QuerySuccessListener listener){
        if(databaseInstance != null) {
            final QuerySuccessListener finalListener = listener;
            new Thread(new Runnable() {
                public void run() {
                    List<Event> returnedValue = EventDAOImpl.getInstance().getByOfflineId(offlineId);
                    if (finalListener != null) {
                        finalListener.onQueryResolved(returnedValue);
                    }
                }
            }).start();
        }
    }

    public void getFirstId(QuerySuccessListener listener){
        if(databaseInstance != null) {
            final QuerySuccessListener finalListener = listener;
            new Thread(new Runnable() {
                public void run() {
                    int returnedValue = EventDAOImpl.getInstance().getFirstId();
                    if (finalListener != null) {
                        finalListener.onQueryResolved(returnedValue);
                    }
                }
            }).start();
        }
    }

    /*public void deleteEvents(List<Event> events, QuerySuccessListener listener){
        final List<Event> finalEvents = events;
        if(databaseInstance != null) {
            final QuerySuccessListener finalListener = listener;
            new Thread(new Runnable() {
                public void run() {
                    int returnedValue = EventDAOImpl.getInstance().deleteEvents(finalEvents);
                    if (finalListener != null) {
                        finalListener.onQueryResolved(returnedValue);
                    }
                }
            }).start();
        }
    }*/

    public void deleteEvents(int offlineId, QuerySuccessListener listener){
        final int finalOfflineId = offlineId;
        if(databaseInstance != null) {
            new Thread(new Runnable() {
                public void run() {
                    int returnedValue = EventDAOImpl.getInstance().deleteEvents(finalOfflineId);
                    EventDAOImpl.getInstance().deleteEvents(finalOfflineId);
                }
            }).start();
        }
    }

    /**
     * Adds a success listener.
     *
     * @param listener the listener to add.
     */
    public void addOnSuccessListener(EventDataSource.QuerySuccessListener listener) {
        successListenerList.add(listener);
    }

    /**
     * Removes a success listener
     *
     * @param listener the listener ot remove
     * @return whether the listener has been removed or not.
     */
    public boolean removeOnSuccessListener(EventDataSource.QuerySuccessListener listener) {
        return successListenerList.remove(listener);
    }

    /**
     * Adds an error listener.
     *
     * @param listener the listener to add
     */
    public void addOnErrorListener(EventDataSource.QueryUnsuccessListener listener) {
        errorListenerList.add(listener);
    }

    /**
     * Remove an error listener
     *
     * @param listener the listener to remove
     * @return whether the listener has been removed or not.
     */
    public boolean removeOnErrorListener(EventDataSource.QueryUnsuccessListener listener) {
        return errorListenerList.remove(listener);
    }

    /**
     * Successful query listener.
     *
     * @see #addOnSuccessListener(EventDataSource.QuerySuccessListener)
     */
    public interface QuerySuccessListener {
        /**
         * Callback invoked when a query from {@link com.npaw.youbora.lib6.persistence.dao.EventDAO} has been successful.
         *
         * @param queryResult expected query return value
         */
        void onQueryResolved(Object queryResult);
    }

    /**
     * Unsuccessful query listener.
     *
     * @see #addOnErrorListener(EventDataSource.QueryUnsuccessListener)
     */
    public interface QueryUnsuccessListener {
        /**
         * Callback invoked when a query from {@link com.npaw.youbora.lib6.persistence.dao.EventDAO} has not been successful.
         *
         * @param exception exception raised
         */
        void onQueryUnresolved(Exception exception);
    }
}
