package com.npaw.youbora.lib6.persistence.dao;

import android.database.sqlite.SQLiteDatabase;

import com.npaw.youbora.lib6.persistence.entity.Event;
import com.npaw.youbora.lib6.persistence.helper.EventDbHelper;

import java.util.List;

/**
 * Created by Enrique on 26/01/2018.
 */

public interface EventDAO {

    long insertNewEvent(Event event);

    List<Event> getAll();

    int getLastId();

    List<Event> getByOfflineId(int offlineId);

    int getFirstId();

    int deleteEvents(int offlineId);

    int deleteAll();

    SQLiteDatabase getReadableDatabase();

    SQLiteDatabase getWritableDatabase();
}
