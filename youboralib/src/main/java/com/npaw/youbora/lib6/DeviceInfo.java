package com.npaw.youbora.lib6;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Utility class to obtain device information propperly, all of its methods are static
 */
public class DeviceInfo{
    /**
     * Get deice model.
     * @return device model.
     */
    public static String getModel(){
        return android.os.Build.MODEL;
    }
    /**
     * Get OS Version.
     * @return readable android version.
     */
    public static String getOSVersion(){
        return android.os.Build.VERSION.RELEASE;
    }
    /**
     * Get deice brand.
     * @return device brand.
     */
    public static String getBrand(){
        return android.os.Build.BRAND;
    }

    public static JSONObject mapToJSON() throws JSONException{
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("model",getModel());
        jsonObject.put("osversion",getOSVersion());
        jsonObject.put("brand",getBrand());
        return jsonObject;
    }
}
