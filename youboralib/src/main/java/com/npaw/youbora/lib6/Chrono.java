package com.npaw.youbora.lib6;

/**
 * Utility class that provides chronometer like functionality.
 * Used to calculate the elapsed time between {@link #start()} and {@link #stop()} calls.
 * @author      Nice People at Work
 * @since       6.0
 */
public class Chrono
{
    /**
     * Start time
     */
    private Long startTime;

    /**
     * Stop time
     */
    private Long stopTime;

    /**
     * Offset to be added to deltaTime and stop. in ms.
     */
    private Long offset;

    /**
     * Constructor
     */
    public Chrono(){
        reset();
    }

    /**
     * Starts this Chronometer.
     *
     * If the chronometer was already started, calling this method restarts it.
     */
    public void start()
    {
        setStartTime(getNow());
        setStopTime(null);
    }

    /**
     * Stop this Chronometer.
     *
     * @return the total milliseconds that happened between the last call to `start()` method and now.
     * If the chronometer was not started before then a -1 is returned.
     */
    public long stop()
    {
        setStopTime(getNow());

        return getDeltaTime();
    }

    /**
     * This method returns the total milliseconds that have passed between the {@link #start()} call and the current time.
     * also it stops the Chrono, use {@link #getDeltaTime(boolean)} with a false param in case one would just know
     * the difference time without stopping the chronometer.
     *
     * @return the total milliseconds that happened between the last call to {@link #start()} method and the time that this
     * chronometer has been stopped. If the chronometer was not started before then a -1 is returned.
     */
    public long getDeltaTime()
    {
        return getDeltaTime(true);
    }

    /**
     * This method returns the total milliseconds that have passed between the {@link #start()} call and the current time.
     * This method will stop the current chronometer depending on the value of the parameter stopIfNeeded.
     *
     * If the chronometer has been already stopped, the stopIfNeeded parameter will have no effect.
     *
     * @param stopIfNeeded boolean value indicating whether this query should stop the chronometer or not.
     * @return the total milliseconds that happened between the last call to {@link #start()} method and the time that this
     * chronometer has been stopped. If the chronometer was not started before then -1 is returned.
     */
    public long getDeltaTime(boolean stopIfNeeded)
    {
        if (getStartTime() == null) {
            return -1;
        }

        if (getStopTime() == null) {
            return stopIfNeeded ? stop() : getDeltaMsec(getStartTime(), getNow()) + getOffset();
        } else {
            return getDeltaMsec(getStartTime(), getStopTime()) + getOffset();
        }
    }

    /**
     * Gett for start time
     * @return the start time
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * Setter for start time
     * @param startTime the time to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * Getter for the stop time
     * @return the stop time
     */
    public Long getStopTime() {
        return stopTime;
    }

    /**
     * Setter for stop time
     * @param stopTime the time to set
     */
    public void setStopTime(Long stopTime) {
        this.stopTime = stopTime;
    }

    /**
     * Gets a timestamp in milliseconds. This is NOT the same as {@link System#currentTimeMillis()}.
     * @return timestamp in milliseconds
     */
    public static long getNow() {
        return System.nanoTime() / 1000000;
    }

    /**
     * Computes the difference between two timestamps
     * @param startMillis Start timestamp in milliseconds
     * @param endMillis End timestamp in milliseconds
     * @return the difference between the two timestamps
     */
    private static long getDeltaMsec(long startMillis, long endMillis) {
        return (endMillis - startMillis);
    }

    /**
     * Reset the {@link Chrono} to its initial state.
     */
    public void reset() {
        startTime = null;
        stopTime = null;
        setOffset(0L);
    }

    /**
     * Creates and returns a copy of the current Chrono.
     * @return a copy of the Chrono
     */
    public Chrono copy() {
        Chrono c = new Chrono();
        c.setStartTime(startTime);
        c.setStopTime(stopTime);
        c.setOffset(offset);
        return c;
    }

    /**
     * Offset to be added to deltaTime and stop. In milliseconds.
     * @return the current offset
     */
    public Long getOffset() {
        return offset;
    }

    /**
     * Sets the offset
     * @param offset to set
     */
    public void setOffset(Long offset) {
        this.offset = offset;
    }
}
