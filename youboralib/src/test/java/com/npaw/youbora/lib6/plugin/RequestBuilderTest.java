package com.npaw.youbora.lib6.plugin;

import com.npaw.youbora.lib6.Constants;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class RequestBuilderTest {

    private RequestBuilder builder;
    private Plugin mockPlugin;

    private static final String [] ALL_PARAMS = new String[] {"playhead", "playrate", "fps", "droppedFrames",
            "mediaDuration", "bitrate", "throughput", "rendition", "title", "title2", "live",
            "mediaResource", "transactionCode", "properties", "playerVersion", "player", "cdn",
            "pluginVersion", "param1", "param2", "param3", "param4", "param5", "param6",
            "param7", "param8", "param9", "param10", "adPosition", "adPlayhead", "adDuration",
            "adBitrate", "adTitle", "adResource", "adPlayerVersion", "adProperties",
            "adAdapterVersion", "pluginInfo", "isp", "connectionType", "ip", "deviceCode",
            "system", "accountCode", "username", "preloadDuration", "joinDuration",
            "bufferDuration", "seekDuration", "pauseDuration", "adJoinDuration",
            "adBufferDuration", "adPauseDuration", "adTotalDuration", "nodeHost", "nodeType",
            "nodeTypeString"};

    @Before
    public void setUp() {
        mockPlugin = mock(Plugin.class);
        builder = spy(new RequestBuilder(mockPlugin));

        // Mocks
        when(mockPlugin.getPlayhead()).thenReturn(1.0);
        when(mockPlugin.getPlayrate()).thenReturn(2.0);
        when(mockPlugin.getFramesPerSecond()).thenReturn(3.0);
        when(mockPlugin.getDroppedFrames()).thenReturn(4);
        when(mockPlugin.getDuration()).thenReturn(5.0);
        when(mockPlugin.getBitrate()).thenReturn(6L);
        when(mockPlugin.getThroughput()).thenReturn(7L);
        when(mockPlugin.getRendition()).thenReturn("a");
        when(mockPlugin.getTitle()).thenReturn("b");
        when(mockPlugin.getTitle2()).thenReturn("c");
        when(mockPlugin.getIsLive()).thenReturn(true);
        when(mockPlugin.getResource()).thenReturn("d");
        when(mockPlugin.getTransactionCode()).thenReturn("e");
        when(mockPlugin.getContentMetadata()).thenReturn("f");
        when(mockPlugin.getPlayerVersion()).thenReturn("g");
        when(mockPlugin.getPlayerName()).thenReturn("h");
        when(mockPlugin.getCdn()).thenReturn("i");
        when(mockPlugin.getPluginVersion()).thenReturn("j");
        when(mockPlugin.getExtraparam1()).thenReturn("j");
        when(mockPlugin.getExtraparam2()).thenReturn("l");
        when(mockPlugin.getExtraparam3()).thenReturn("m");
        when(mockPlugin.getExtraparam4()).thenReturn("n");
        when(mockPlugin.getExtraparam5()).thenReturn("o");
        when(mockPlugin.getExtraparam6()).thenReturn("p");
        when(mockPlugin.getExtraparam7()).thenReturn("q");
        when(mockPlugin.getExtraparam8()).thenReturn("r");
        when(mockPlugin.getExtraparam9()).thenReturn("s");
        when(mockPlugin.getExtraparam10()).thenReturn("t");
        when(mockPlugin.getAdPosition()).thenReturn("u");
        when(mockPlugin.getAdPlayhead()).thenReturn(8.0);
        when(mockPlugin.getAdDuration()).thenReturn(9.0);
        when(mockPlugin.getAdBitrate()).thenReturn(10L);
        when(mockPlugin.getAdTitle()).thenReturn("w");
        when(mockPlugin.getAdResource()).thenReturn("x");
        when(mockPlugin.getAdPlayerVersion()).thenReturn("y");
        when(mockPlugin.getAdMetadata()).thenReturn("z");
        when(mockPlugin.getAdAdapterVersion()).thenReturn("aa");
        when(mockPlugin.getPluginInfo()).thenReturn("ab");
        when(mockPlugin.getIsp()).thenReturn("ac");
        when(mockPlugin.getConnectionType()).thenReturn("ad");
        when(mockPlugin.getIp()).thenReturn("ae");
        when(mockPlugin.getDeviceCode()).thenReturn("af");
        when(mockPlugin.getAccountCode()).thenReturn("agah");
        when(mockPlugin.getUsername()).thenReturn("ai");
        when(mockPlugin.getPreloadDuration()).thenReturn(11L);
        when(mockPlugin.getJoinDuration()).thenReturn(12L);
        when(mockPlugin.getBufferDuration()).thenReturn(13L);
        when(mockPlugin.getSeekDuration()).thenReturn(14L);
        when(mockPlugin.getPauseDuration()).thenReturn(15L);
        when(mockPlugin.getAdJoinDuration()).thenReturn(16L);
        when(mockPlugin.getAdBufferDuration()).thenReturn(17L);
        when(mockPlugin.getAdPauseDuration()).thenReturn(18L);
        when(mockPlugin.getAdTotalDuration()).thenReturn(19L);
        when(mockPlugin.getNodeHost()).thenReturn("aj");
        when(mockPlugin.getNodeType()).thenReturn("ak");
        when(mockPlugin.getNodeTypeString()).thenReturn("al");
    }

    @Test
    public void testBuildParamsFetch() {
        // Test that buildparams fetches the corresponding params
        builder.buildParams(null, Constants.SERVICE_JOIN);

        verify(builder, atLeastOnce()).fetchParams(anyMap(), eq(new String[] {"joinDuration", "playhead", "mediaDuration"}), eq(false));
        verify(builder, atLeastOnce()).fetchParams(anyMap(), eq(new String[] {"title", "title2", "live", "mediaDuration", "mediaResource"}), eq(true));

    }

    @Test
    public void testParamsFetchedFromPlugin() {

        Map<String, String> params = builder.fetchParams(null, ALL_PARAMS, false);

        assertEquals("1.0", params.get("playhead"));
        assertEquals("2.0", params.get("playrate"));
        assertEquals("3.0", params.get("fps"));
        assertEquals("4", params.get("droppedFrames"));
        assertEquals("5.0", params.get("mediaDuration"));
        assertEquals("6", params.get("bitrate"));
        assertEquals("7", params.get("throughput"));
        assertEquals("a", params.get("rendition"));
        assertEquals("b", params.get("title"));
        assertEquals("c", params.get("title2"));
        assertEquals("true", params.get("live"));
        assertEquals("d", params.get("mediaResource"));
        assertEquals("e", params.get("transactionCode"));
        assertEquals("f", params.get("properties"));
        assertEquals("g", params.get("playerVersion"));
        assertEquals("h", params.get("player"));
        assertEquals("i", params.get("cdn"));
        assertEquals("j", params.get("pluginVersion"));
        assertEquals("j", params.get("param1"));
        assertEquals("l", params.get("param2"));
        assertEquals("m", params.get("param3"));
        assertEquals("n", params.get("param4"));
        assertEquals("o", params.get("param5"));
        assertEquals("p", params.get("param6"));
        assertEquals("q", params.get("param7"));
        assertEquals("r", params.get("param8"));
        assertEquals("s", params.get("param9"));
        assertEquals("t", params.get("param10"));
        assertEquals("u", params.get("adPosition"));
        assertEquals("8.0", params.get("adPlayhead"));
        assertEquals("9.0", params.get("adDuration"));
        assertEquals("10", params.get("adBitrate"));
        assertEquals("w", params.get("adTitle"));
        assertEquals("x", params.get("adResource"));
        assertEquals("y", params.get("adPlayerVersion"));
        assertEquals("z", params.get("adProperties"));
        assertEquals("aa", params.get("adAdapterVersion"));
        assertEquals("ab", params.get("pluginInfo"));
        assertEquals("ac", params.get("isp"));
        assertEquals("ad", params.get("connectionType"));
        assertEquals("ae", params.get("ip"));
        assertEquals("af", params.get("deviceCode"));
        assertEquals("agah", params.get("system"));
        assertEquals("agah", params.get("accountCode"));
        assertEquals("ai", params.get("username"));
        assertEquals("11", params.get("preloadDuration"));
        assertEquals("12", params.get("joinDuration"));
        assertEquals("13", params.get("bufferDuration"));
        assertEquals("14", params.get("seekDuration"));
        assertEquals("15", params.get("pauseDuration"));
        assertEquals("16", params.get("adJoinDuration"));
        assertEquals("17", params.get("adBufferDuration"));
        assertEquals("18", params.get("adPauseDuration"));
        assertEquals("19", params.get("adTotalDuration"));
        assertEquals("aj", params.get("nodeHost"));
        assertEquals("ak", params.get("nodeType"));
        assertEquals("al", params.get("nodeTypeString"));

    }

    @Test
    public void testAdNumber() {
        // Prerolls
        when(mockPlugin.getAdPosition()).thenReturn("pre");

        for (int i = 1; i<=10; i++) {
            assertEquals(Integer.toString(i), builder.getNewAdNumber());
            builder.fetchParams(null, new String[]{"adPosition"}, false);
        }

        // Midrolls
        when(mockPlugin.getAdPosition()).thenReturn("mid");

        for (int i = 1; i<=10; i++) {
            assertEquals(Integer.toString(i), builder.getNewAdNumber());
            builder.fetchParams(null, new String[]{"adPosition"}, false);
        }


        // Postrolls
        when(mockPlugin.getAdPosition()).thenReturn("post");

        for (int i = 1; i<=10; i++) {
            assertEquals(Integer.toString(i), builder.getNewAdNumber());
            builder.fetchParams(null, new String[]{"adPosition"}, false);
        }
    }

    @Test
    public void testInformedParamsAreNotOverwritten() {

        Map<String, String> params = new HashMap<>(3);
        params.put("playhead", "informedPlayhead");
        params.put("playrate", "informedPlayrate");
        params.put("nodeTypeString", "informedNodeTypeString");

        params = builder.fetchParams(params, ALL_PARAMS, false);

        assertEquals("informedPlayhead", params.get("playhead"));
        assertEquals("informedPlayrate", params.get("playrate"));
        assertEquals("informedNodeTypeString", params.get("nodeTypeString"));
    }

    @Test
    public void testChangedEntities() {

        builder.fetchParams(null, ALL_PARAMS, false);

        Map<String, String> params = builder.getChangedEntities();

        assertEquals(0, params.size());

        when(mockPlugin.getResource()).thenReturn("newResource");
        when(mockPlugin.getDuration()).thenReturn(234.0);

        params = builder.getChangedEntities();

        assertEquals(2, params.size());

        assertEquals("newResource", params.get("mediaResource"));
        assertEquals("234.0", params.get("mediaDuration"));
    }
}