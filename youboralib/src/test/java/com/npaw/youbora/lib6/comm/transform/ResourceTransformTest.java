package com.npaw.youbora.lib6.comm.transform;

import android.os.Handler;

import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.comm.Request;
import com.npaw.youbora.lib6.comm.transform.resourceparse.CdnParser;
import com.npaw.youbora.lib6.comm.transform.resourceparse.HlsParser;
import com.npaw.youbora.lib6.comm.transform.resourceparse.cdn.CdnTypeParser;
import com.npaw.youbora.lib6.plugin.Plugin;
import com.npaw.youbora.lib6.plugin.RequestBuilder;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class ResourceTransformTest {

    private HlsParser mockHlsParser;
    private Handler mockHandler;
    private CdnParser mockCdnParser;

    private ResourceTransform createTransformWithMocks(Plugin mockPlugin) {

        ResourceTransform resourceTransform = spy(new ResourceTransform(mockPlugin));

        mockHlsParser = mock(HlsParser.class);
        when(resourceTransform.createHlsParser()).thenReturn(mockHlsParser);

        mockCdnParser = mock(CdnParser.class);
        when(resourceTransform.createCdnParser(anyString())).thenReturn(mockCdnParser);

        mockHandler = mock(Handler.class);
        when(resourceTransform.createHandler()).thenReturn(mockHandler);

        return resourceTransform;
    }

    @Test
    public void testDefaultValues() {
        Plugin mockPlugin = mock(Plugin.class);

        ResourceTransform resourceTransform = new ResourceTransform(mockPlugin);

        // Assert default values
        assertNull(resourceTransform.getCdnName());
        assertNull(resourceTransform.getNodeHost());
        assertNull(resourceTransform.getNodeType());
        assertNull(resourceTransform.getNodeTypeString());
        assertNull(resourceTransform.getResource());
    }

    @Test
    public void testFullFlow() throws Exception {
        // Mocks
        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.isParseCdnNode()).thenReturn(true);
        when(mockPlugin.isParseHls()).thenReturn(true);
        when(mockPlugin.getParseCdnNodeNameHeader()).thenReturn("header-name");
        List<String> cdns = new ArrayList<>();
        cdns.add("cdn1");
        cdns.add("cdn2");
        when(mockPlugin.getParseCdnNodeList()).thenReturn(cdns);

        // Resource transform to test
        ResourceTransform resourceTransform = createTransformWithMocks(mockPlugin);

        assertFalse(resourceTransform.isBlocking(null));

        // Init, this should start hls parsing
        resourceTransform.init("resource");

        assertTrue(resourceTransform.isBlocking(null));

        verify(mockHlsParser, atLeastOnce()).parse(eq("resource"), (String) eq(null));

        // Capture callback
        ArgumentCaptor<HlsParser.HlsTransformListener> captor = ArgumentCaptor.forClass(HlsParser.HlsTransformListener.class);
        verify(mockHlsParser, times(1)).addHlsTransformListener(captor.capture());

        // Prepare mocks for the cdn
        CdnParser mockCdnParser1 = mock(CdnParser.class);
        CdnParser mockCdnParser2 = mock(CdnParser.class);
        when(resourceTransform.createCdnParser(eq("cdn1"))).thenReturn(mockCdnParser1);
        when(resourceTransform.createCdnParser(eq("cdn2"))).thenReturn(mockCdnParser2);

        // Invoke callback
        captor.getValue().onHlsTransformDone("parsed-resource");

        // That should have updated the resource
        assertEquals("parsed-resource", resourceTransform.getResource());

        // Mock cdn values
        when(mockCdnParser2.getCdnName()).thenReturn("parsedCdnName");
        when(mockCdnParser2.getNodeHost()).thenReturn("parsedNodeHost");
        when(mockCdnParser2.getNodeType()).thenReturn(CdnTypeParser.Type.Hit);
        when(mockCdnParser2.getNodeTypeString()).thenReturn("HIT");

        // Capture cdn parser callback
        ArgumentCaptor<CdnParser.CdnTransformListener> cdnCaptor1 = ArgumentCaptor.forClass(CdnParser.CdnTransformListener.class);
        verify(mockCdnParser1, times(1)).addCdnTransformListener(cdnCaptor1.capture());

        // Invoke callback, as the mockedCdn will return null, the second one will be called
        cdnCaptor1.getValue().onCdnTransformDone(mockCdnParser1);

        ArgumentCaptor<CdnParser.CdnTransformListener> cdnCaptor2 = ArgumentCaptor.forClass(CdnParser.CdnTransformListener.class);
        verify(mockCdnParser2, times(1)).addCdnTransformListener(cdnCaptor2.capture());

        // Invoke callback. This time the cdn will provide info
        cdnCaptor2.getValue().onCdnTransformDone(mockCdnParser2);

        assertFalse(resourceTransform.isBlocking(null));

        // Check parsed values
        assertEquals("parsedCdnName", resourceTransform.getCdnName());
        assertEquals("parsedNodeHost", resourceTransform.getNodeHost());
        assertEquals("1", resourceTransform.getNodeType());
        assertEquals("HIT", resourceTransform.getNodeTypeString());
        assertEquals("parsed-resource", resourceTransform.getResource());

        // Check parse start request
        Request mockStart = mock(Request.class);
        when(mockStart.getService()).thenReturn(Constants.SERVICE_START);

        // Mocks
        RequestBuilder mockBuilder = mock(RequestBuilder.class);
        Map<String, String> lastSent = mock(HashMap.class);
        when(mockBuilder.getLastSent()).thenReturn(lastSent);
        when(mockPlugin.getRequestBuilder()).thenReturn(mockBuilder);

        resourceTransform.parse(mockStart);

        verify(mockStart, times(1)).setParam(eq("mediaResource"), eq("parsed-resource"));
        verify(mockStart, times(1)).setParam(eq("cdn"), eq("parsedCdnName"));
        verify(mockStart, times(1)).setParam(eq("nodeHost"), eq("parsedNodeHost"));
        verify(mockStart, times(1)).setParam(eq("nodeType"), eq("1"));
        verify(mockStart, times(1)).setParam(eq("nodeTypeString"), eq("HIT"));

        verify(lastSent, times(1)).put(eq("mediaResource"), eq("parsed-resource"));
        verify(lastSent, times(1)).put(eq("cdn"), eq("parsedCdnName"));
        verify(lastSent, times(1)).put(eq("nodeHost"), eq("parsedNodeHost"));
        verify(lastSent, times(1)).put(eq("nodeType"), eq("1"));
        verify(lastSent, times(1)).put(eq("nodeTypeString"), eq("HIT"));
    }

    @Test
    public void testNothingEnabled() throws Exception {
        // Mocks
        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.isParseCdnNode()).thenReturn(false);
        when(mockPlugin.isParseHls()).thenReturn(false);

        // Resource transform to test
        ResourceTransform resourceTransform = createTransformWithMocks(mockPlugin);

        assertFalse(resourceTransform.isBlocking(null));

        resourceTransform.init("resource");

        assertFalse(resourceTransform.isBlocking(null));

    }

    @Test
    public void testStopOnTimeout() throws Exception {

        // Mocks
        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.isParseCdnNode()).thenReturn(true);
        when(mockPlugin.isParseHls()).thenReturn(true);

        // Resource transform to test
        ResourceTransform resourceTransform = createTransformWithMocks(mockPlugin);

        assertFalse(resourceTransform.isBlocking(null));

        resourceTransform.init("resource");

        assertTrue(resourceTransform.isBlocking(null));

        // Capture timeout callback
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(mockHandler, times(1)).postDelayed(captor.capture(), eq(3000L));

        captor.getValue().run();

        assertFalse(resourceTransform.isBlocking(null));
    }

}