package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.Constants;
import com.npaw.youbora.lib6.comm.Request;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class Nqs6TransformTest {

    private Nqs6Transform transform;

    @Before
    public void setUp() {
        transform = new Nqs6Transform();
    }

    @After
    public void tearDown() {
        transform = null;
    }

    @Test
    public void testParamCloneNull() {
        Request mockRequest = mock(Request.class);
        when(mockRequest.getService()).thenReturn(null);

        transform.parse(mockRequest);

        when(mockRequest.getService()).thenReturn("");

        transform.parse(mockRequest);

        verify(mockRequest, times(2)).getService();

    }

    @Test
    public void testParamCloneStart() {
        Request mockRequest = mock(Request.class);
        when(mockRequest.getService()).thenReturn(Constants.SERVICE_START);

        mockGenericParams(mockRequest);

        when(mockRequest.getParam("mediaDuration")).thenReturn("1");

        transform.parse(mockRequest);

        checkGenericParams(mockRequest);
        verify(mockRequest, times(1)).setParam("duration", "1");
    }

    @Test
    public void testParamCloneJoin() {
        Request mockRequest = mock(Request.class);
        when(mockRequest.getService()).thenReturn(Constants.SERVICE_JOIN);

        when(mockRequest.getParam("joinDuration")).thenReturn("1");
        when(mockRequest.getParam("playhead")).thenReturn("2");

        transform.parse(mockRequest);

        verify(mockRequest, times(1)).setParam("time", "1");
        verify(mockRequest, times(1)).setParam("eventTime", "2");
    }

    @Test
    public void testParamCloneSeek() {
        Request mockRequest = mock(Request.class);
        when(mockRequest.getService()).thenReturn(Constants.SERVICE_SEEK);

        mockGenericParams(mockRequest);

        when(mockRequest.getParam("seekDuration")).thenReturn("1");

        transform.parse(mockRequest);

        checkGenericParams(mockRequest);
        verify(mockRequest, times(1)).setParam("duration", "1");
    }

    @Test
    public void testParamCloneBuffer() {
        Request mockRequest = mock(Request.class);
        when(mockRequest.getService()).thenReturn(Constants.SERVICE_BUFFER);

        mockGenericParams(mockRequest);

        when(mockRequest.getParam("bufferDuration")).thenReturn("1");

        transform.parse(mockRequest);

        checkGenericParams(mockRequest);
        verify(mockRequest, times(1)).setParam("duration", "1");
    }

    @Test
    public void testParamClonePing() {
        Request mockRequest = mock(Request.class);
        when(mockRequest.getService()).thenReturn(Constants.SERVICE_PING);

        mockGenericParams(mockRequest);

        when(mockRequest.getParam("entities")).thenReturn("{\"entity1\":\"value1\",\"entity2\":\"value2\"}");

        transform.parse(mockRequest);

        checkGenericParams(mockRequest);
        verify(mockRequest, times(1)).setParam("entityType", "entity1");
        verify(mockRequest, times(1)).setParam("entityValue", "value1");
    }

    private void mockGenericParams(Request mockRequest) {
        when(mockRequest.getParam("accountCode")).thenReturn("1");
        when(mockRequest.getParam("transactionCode")).thenReturn("2");
        when(mockRequest.getParam("username")).thenReturn("3");
        when(mockRequest.getParam("mediaResource")).thenReturn("4");
        when(mockRequest.getParam("errorMsg")).thenReturn("5");
        when(mockRequest.getParam("playhead")).thenReturn("6");
    }

    private void checkGenericParams(Request mockRequest) {
        verify(mockRequest, times(1)).setParam("system", "1");
        verify(mockRequest, times(1)).setParam("transcode", "2");
        verify(mockRequest, times(1)).setParam("user", "3");
        verify(mockRequest, times(1)).setParam("resource", "4");
        verify(mockRequest, times(1)).setParam("msg", "5");
        verify(mockRequest, times(1)).setParam("time", "6");
    }
}