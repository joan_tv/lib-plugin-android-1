package com.npaw.youbora.lib6;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by rufo on 26/12/2017.
 */

public class ConstantsTest {

    @Test
    public void testAllConstantsValues() {
        assertEquals("/data",Constants.SERVICE_DATA);
        assertEquals("/init",Constants.SERVICE_INIT);
        assertEquals("/start",Constants.SERVICE_START);
        assertEquals("/joinTime",Constants.SERVICE_JOIN);
        assertEquals("/pause",Constants.SERVICE_PAUSE);
        assertEquals("/resume",Constants.SERVICE_RESUME);
        assertEquals("/seek",Constants.SERVICE_SEEK);
        assertEquals("/bufferUnderrun",Constants.SERVICE_BUFFER);
        assertEquals("/error",Constants.SERVICE_ERROR);
        assertEquals("/stop",Constants.SERVICE_STOP);
        assertEquals("/ping",Constants.SERVICE_PING);
        assertEquals("/offlineEvents",Constants.SERVICE_OFFLINE_EVENTS);
        assertEquals("/adInit",Constants.SERVICE_AD_INIT);
        assertEquals("/adStart",Constants.SERVICE_AD_START);
        assertEquals("/adJoin",Constants.SERVICE_AD_JOIN);
        assertEquals("/adClick",Constants.SERVICE_AD_CLICK);
        assertEquals("/adPause",Constants.SERVICE_AD_PAUSE);
        assertEquals("/adResume",Constants.SERVICE_AD_RESUME);
        assertEquals("/adBufferUnderrun",Constants.SERVICE_AD_BUFFER);
        assertEquals("/adStop",Constants.SERVICE_AD_STOP);
        assertEquals("/adError",Constants.SERVICE_AD_ERROR);
    }
}
