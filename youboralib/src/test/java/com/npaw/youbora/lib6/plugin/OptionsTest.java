package com.npaw.youbora.lib6.plugin;

import android.os.Bundle;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class OptionsTest {
    private Options o;
    private Bundle optBundle;
    private Map<String, Object> fakeBundle;

    @Before
    public void setUp() throws Exception {
        o = new Options();
        optBundle = mock(Bundle.class);
        fakeBundle = new HashMap<>();

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Object[] arguments = invocationOnMock.getArguments();
                String key = ((String) arguments[0]);
                Boolean value = ((Boolean) arguments[1]);
                fakeBundle.put(key, value);
                return null;
            }
        }).when(optBundle).putBoolean(anyString(),anyBoolean());

        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Object[] arguments = invocationOnMock.getArguments();
                String key = ((String) arguments[0]);
                String value = ((String) arguments[1]);
                fakeBundle.put(key, value);
                return null;
            }
        }).when(optBundle).putString(anyString(),anyString());


        when(optBundle.getString(anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                Object[] arguments = invocation.getArguments();
                String key = ((String) arguments[0]);
                return (String)fakeBundle.get(key);
            }
        });


        when(optBundle.getBoolean(anyString())).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocation) throws Throwable {
                Object[] arguments = invocation.getArguments();
                String key = ((String) arguments[0]);
                return (Boolean)fakeBundle.get(key);
            }
        });

    }

    @After
    public void tearDown() throws Exception {
        o = null;
    }

    @Test
    public void testOptions() {
        //Setting options using set method
        setValues(o);
        assertValues(o);

        //Setting options using Bundle
        setBundleAsOptions();
    }

    private void setValues(Options o) {

        // Set values
        o.setEnabled(false);
        o.setHttpSecure(true);
        o.setHost("a");
        o.setAccountCode("b");
        o.setUsername("c");
        o.setParseHls(true);
        o.setParseCdnNameHeader("d");
        o.setParseCdnNode(true);
        ArrayList<String> cdnNodes = new ArrayList<>();
        cdnNodes.add("listitem1");
        cdnNodes.add("listitem2");
        o.setParseCdnNodeList(cdnNodes);
        o.setNetworkIP("f");
        o.setNetworkIsp("g");
        o.setNetworkConnectionType("h");
        o.setDeviceCode("i");
        o.setContentResource("j");
        o.setContentIsLive(true);
        o.setContentTitle("k");
        o.setContentTitle2("l");
        o.setContentDuration(1.0);
        o.setContentTransactionCode("m");
        o.setContentBitrate(2L);
        o.setContentThroughput(3L);
        o.setContentRendition("n");
        o.setContentCdn("o");
        o.setContentFps(4.0);
        Bundle b = mock(Bundle.class);
        when(b.getString("p")).thenReturn("q");
        b.putString("p", "q");
        o.setContentMetadata(b);
        Bundle b2 = mock(Bundle.class);
        when(b2.getString("r")).thenReturn("s");
        b.putString("r", "s");
        o.setAdMetadata(b2);
        o.setAdIgnore(false);
        o.setAdsAfterStop(0);
        o.setAutoDetectBackground(false);
        o.setAutoStart(true);
        o.setOffline(false);
        o.setExtraparam1("t");
        o.setExtraparam2("u");
        o.setExtraparam3("v");
        o.setExtraparam4("w");
        o.setExtraparam5("x");
        o.setExtraparam6("y");
        o.setExtraparam7("z");
        o.setExtraparam8("aa");
        o.setExtraparam9("ab");
        o.setExtraparam10("ac");
        o.setExtraparam11("ad");
        o.setExtraparam12("ae");
        o.setExtraparam13("af");
        o.setExtraparam14("ag");
        o.setExtraparam15("ah");
        o.setExtraparam16("ai");
        o.setExtraparam17("aj");
        o.setExtraparam18("ak");
        o.setExtraparam19("al");
        o.setExtraparam20("am");
        o.setUserType("an");
        o.setContentStreamingProtocol("DASH");
    }

    private void assertValues(Options o) {

        // Verify
        assertEquals(false, o.isEnabled());
        assertEquals(true, o.isHttpSecure());
        assertEquals("a", o.getHost());
        assertEquals("b", o.getAccountCode());
        assertEquals("c", o.getUsername());
        assertEquals(true, o.isParseHls());
        assertEquals("d", o.getParseCdnNameHeader());
        assertEquals(true, o.isParseCdnNode());
        assertEquals("listitem1", o.getParseCdnNodeList().get(0));
        assertEquals("listitem2", o.getParseCdnNodeList().get(1));
        assertEquals("f", o.getNetworkIP());
        assertEquals("g", o.getNetworkIsp());
        assertEquals("h", o.getNetworkConnectionType());
        assertEquals("i", o.getDeviceCode());
        assertEquals("j", o.getContentResource());
        assertEquals(true, o.getContentIsLive());
        assertEquals("k", o.getContentTitle());
        assertEquals("l", o.getContentTitle2());
        assertEquals((Double) 1.0, o.getContentDuration());
        assertEquals("m", o.getContentTransactionCode());
        assertEquals((Long) 2L, o.getContentBitrate());
        assertEquals((Long) 3L, o.getContentThroughput());
        assertEquals("n", o.getContentRendition());
        assertEquals("o", o.getContentCdn());
        assertEquals((Double) 4.0, o.getContentFps());
        assertEquals("q", o.getContentMetadata().getString("p"));
        assertEquals("s", o.getAdMetadata().getString("r"));
        assertEquals(false,o.getAdIgnore());
        assertEquals(0,o.getAdsAfterStop());
        assertEquals(false,o.isAutoDetectBackground());
        assertEquals(true,o.isAutoStart());
        assertEquals(false,o.isOffline());
        assertEquals("t", o.getExtraparam1());
        assertEquals("u", o.getExtraparam2());
        assertEquals("v", o.getExtraparam3());
        assertEquals("w", o.getExtraparam4());
        assertEquals("x", o.getExtraparam5());
        assertEquals("y", o.getExtraparam6());
        assertEquals("z", o.getExtraparam7());
        assertEquals("aa", o.getExtraparam8());
        assertEquals("ab", o.getExtraparam9());
        assertEquals("ac", o.getExtraparam10());
        assertEquals("ad", o.getExtraparam11());
        assertEquals("ae", o.getExtraparam12());
        assertEquals("af", o.getExtraparam13());
        assertEquals("ag", o.getExtraparam14());
        assertEquals("ah", o.getExtraparam15());
        assertEquals("ai", o.getExtraparam16());
        assertEquals("aj", o.getExtraparam17());
        assertEquals("ak", o.getExtraparam18());
        assertEquals("al", o.getExtraparam19());
        assertEquals("am", o.getExtraparam20());
        assertEquals("an", o.getUserType());
        assertEquals("DASH",o.getContentStreamingProtocol());
    }

    private void setBundleAsOptions(){

        /*doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                Object[] arguments = invocationOnMock.getArguments();
                String key = ((String) arguments[0]);
                String value = ((String) arguments[1]);
                fakeBundle.put(key, value);
                return null;
            }
        }).when(optBundle).putStringArrayList(anyString(), Matchers.anyListOf(String.class));*/

        ArrayList<String> cdnNodeList = new ArrayList<>();
        cdnNodeList.add("listitem1");
        cdnNodeList.add("listitem2");

        Bundle metadataBundle = mock(Bundle.class);
        metadataBundle.putString("p","q");

        Bundle adMetadataBundle = mock(Bundle.class);
        adMetadataBundle.putString("r","s");

        optBundle.putBoolean("enabled",false);
        optBundle.putBoolean("httpSecure",false);
        optBundle.putString("host","a");
        optBundle.putString("config.accountCode","b");
        optBundle.putString("username","c");
        optBundle.putBoolean("parse.Hls",true);
        optBundle.putString("parse.CdnNameHeader","d");
        optBundle.putBoolean("parse.CdnNode",true);
        optBundle.putStringArrayList("parse.CdnNodeList",cdnNodeList);
        fakeBundle.put("parse.CdnNodeList",cdnNodeList);
        optBundle.putString("network.IP","f");
        optBundle.putString("network.Isp","g");
        optBundle.putString("network.connectionType","h");
        optBundle.putString("device.code","i");
        optBundle.putString("content.resource","j");
        optBundle.putBoolean("content.isLive",true);
        optBundle.putString("content.title","k");
        optBundle.putString("content.title2","l");
        optBundle.putDouble("content.duration",1.0);
        optBundle.putString("content.transactionCode","m");
        optBundle.putLong("content.bitrate",2L);
        optBundle.putLong("content.throughput",3L);
        optBundle.putString("content.rendition","n");
        optBundle.putString("content.cdn","o");
        optBundle.putDouble("content.fps",4.0);
        optBundle.putBundle("content.metadata",metadataBundle);
        fakeBundle.put("content.metadata",metadataBundle);
        optBundle.putBundle("ad.metadata",adMetadataBundle);
        fakeBundle.put("ad.metadata",adMetadataBundle);
        optBundle.putBoolean("ad.ignore",false);
        optBundle.putInt("ad.afterStop",0);
        optBundle.putBoolean("autoDetectBackground",false);
        optBundle.putBoolean("autoStart",true);
        optBundle.putBoolean("offline",false);
        optBundle.putString("extraparam.1","t");
        optBundle.putString("extraparam.2","u");
        optBundle.putString("extraparam.3","v");
        optBundle.putString("extraparam.4","w");
        optBundle.putString("extraparam.5","x");
        optBundle.putString("extraparam.6","y");
        optBundle.putString("extraparam.7","z");
        optBundle.putString("extraparam.8","aa");
        optBundle.putString("extraparam.9","ab");
        optBundle.putString("extraparam.10","ac");
        optBundle.putString("extraparam.11","ad");
        optBundle.putString("extraparam.12","ae");
        optBundle.putString("extraparam.13","af");
        optBundle.putString("extraparam.14","ag");
        optBundle.putString("extraparam.15","ah");
        optBundle.putString("extraparam.16","ai");
        optBundle.putString("extraparam.17","aj");
        optBundle.putString("extraparam.18","ak");
        optBundle.putString("extraparam.19","al");
        optBundle.putString("extraparam.20","am");

        o = new Options(optBundle);

    }
}