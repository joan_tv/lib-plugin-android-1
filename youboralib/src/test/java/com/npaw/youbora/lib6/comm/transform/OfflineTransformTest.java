package com.npaw.youbora.lib6.comm.transform;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.comm.Request;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by rufo on 26/12/2017.
 */

public class OfflineTransformTest {

    private OfflineTransform offlineTransform;
    private Request mockRequest;
    private YouboraLog.YouboraLogger mockLogger;

    @Before
    public void setUp() {

        YouboraLog.setDebugLevel(YouboraLog.Level.SILENT);
        mockLogger = mock(YouboraLog.YouboraLogger.class);
        YouboraLog.addLogger(mockLogger);

        offlineTransform = new OfflineTransform();
        mockRequest = mock(Request.class);
    }

    @After
    public void tearDown() {
        offlineTransform = null;
    }

    @Test
    public void testHasToSend(){
        Request mockRequest = mock(Request.class);

        assertFalse(offlineTransform.hasToSend(mockRequest));
    }

    @Test
    public void testState(){
        assertEquals(offlineTransform.getState(),Transform.STATE_OFFLINE);
    }
}
