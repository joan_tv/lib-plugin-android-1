package com.npaw.youbora.lib6.plugin;

import com.npaw.youbora.lib6.Constants;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PluginInstrumentedTest extends com.npaw.youbora.lib6.plugin.PluginMocker {

    @Test
    public void testPingBasicParams() {

        // Basic ping params
        ArgumentCaptor<List> mapCaptor = ArgumentCaptor.forClass(List.class);
        timerEventListener.onTimerEvent(5000L);
        verify(mockRequestBuilder).buildParams(anyMap(), eq(Constants.SERVICE_PING));
        verify(mockRequestBuilder).fetchParams(anyMap(), mapCaptor.capture(), anyBoolean());
        List<String> params = mapCaptor.getValue();
        verifyBasicParamsPing(params);
    }

    @Test
    public void testPingPaused() {
        mockAdapter.getFlags().setPaused(true);

        ArgumentCaptor<List> mapCaptor = ArgumentCaptor.forClass(List.class);
        timerEventListener.onTimerEvent(5000L);
        verify(mockRequestBuilder).fetchParams(anyMap(), mapCaptor.capture(), anyBoolean());
        List<String> params = mapCaptor.getValue();
        assertTrue(params.contains("pauseDuration"));
    }

    @Test
    public void testPingBuffering() {
        mockAdapter.getFlags().setBuffering(true);

        ArgumentCaptor<List> mapCaptor = ArgumentCaptor.forClass(List.class);
        timerEventListener.onTimerEvent(5000L);
        verify(mockRequestBuilder).fetchParams(anyMap(), mapCaptor.capture(), anyBoolean());
        List<String> params = mapCaptor.getValue();
        verifyBasicParamsPing(params);
        assertTrue(params.contains("bufferDuration"));
    }

    @Test
    public void testPingSeek() {
        mockAdapter.getFlags().setSeeking(true);

        ArgumentCaptor<List> mapCaptor = ArgumentCaptor.forClass(List.class);
        timerEventListener.onTimerEvent(5000L);
        verify(mockRequestBuilder).fetchParams(anyMap(), mapCaptor.capture(), anyBoolean());
        List<String> params = mapCaptor.getValue();
        verifyBasicParamsPing(params);
        assertTrue(params.contains("seekDuration"));
    }

    @Test
    public void testPingAds() {
        mockAdAdapter.getFlags().setStarted(true);
        mockAdAdapter.getFlags().setBuffering(true);

        ArgumentCaptor<List> mapCaptor = ArgumentCaptor.forClass(List.class);
        timerEventListener.onTimerEvent(5000L);
        verify(mockRequestBuilder).fetchParams(anyMap(), mapCaptor.capture(), anyBoolean());
        List<String> params = mapCaptor.getValue();
        verifyBasicParamsPing(params);
        assertTrue(params.contains("adBitrate"));
        assertTrue(params.contains("adPlayhead"));
        assertTrue(params.contains("adBufferDuration"));
    }

    private void verifyBasicParamsPing(List<String> params) {
        assertTrue(params.contains("bitrate"));
        assertTrue(params.contains("throughput"));
        assertTrue(params.contains("fps"));
    }

}