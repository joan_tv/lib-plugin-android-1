## [6.1.5] -
### Fixed
 - Wrong pingTime if sending offline events just before starting offline playback

## [6.1.4] - 2018-03-23
### Added
 - Experiment ids for smart users
### Fixed
 - No exception raised if adapter.getDuration() returns null
 
## [6.1.3] - 2018-03-08
### Added
 - Now there are ten extraparams (total twenty)

## [6.1.2] - 2018-02-21
### Fixed
 - Now if you fire an error without init or start the view number will increase

## [6.1.1] - 2018-02-19
### Improved
 - If is live content duration will be reported as 0
### Fixed
 - Error serverity is not send anymore (adapters can send it anyway)
 - No crash if an ads adapter doesn't implement getPosition() method

## [6.1.0] - 2018-01-30
### Added
 - Streaming protocol option now is "sendable"
 - User Type option
 - Ad extraparams
### Removed
 - Now Room is no longer a dependency

## [6.1.0-beta2] - 2018-01-15
### Fixed
 - Now events are properly removed when they have been send
 - New event send system

## [6.1.0-beta] - 2018-01-05
### Refactored
 - Offline mode refactored, now SQLite is used instead of SharedPrefs

## [6.0.10] - 2018-01-02
### Added
 - Streaming protocol option
 - Test cases

## [6.0.9] - 2017-12-22
### Fixed
 - No data petition in case of offline mode
 - Pause duration properly reported in case of stopping while paused
 - Fire fatal error improved

## [6.0.8] - 2017-12-15
### Added
 - removeAdapter and removeAdsAdapter stopping pings is optional
 - Add AdError specific methods
 - Add playhead
 - Add timemark for debug porpuses
### Fixed
 - If no adInit join starts
 - In case of stop while paused now pauseDuration is send too
 - Only stops if init
## Removed
 - Send ALWAYS ads stop if needed with player stop

## [6.0.8-beta8] - 2017-12-04
### Added
 - Callback to call when ads have ended
 - Timemark on all requests
### Fixed
 - Null check chronos

## [6.0.8-beta7] - 2017-11-29
### Added
 - FireEnd method
### Fixed
 - FireEnd with params
 - adPlayhead and playhead doesn't need adapter

## [6.0.8-beta6] - 2017-11-28
### Added
 - FireFatalError at plugin
 - Check for playback has really ended
### Fixed
 - Wrong join time with adInit.

## [6.0.8-beta5] - 2017-11-24
### Added
 - FireStop plugin
 - Check playhead for ad position

## [6.0.8-beta4] - 2017-11-24
### Added
 - FireFatalError with Exception to filter by it

## [6.0.8-beta3] - 2017-11-23
### Added
 - New error method
###Fixed
 - Wrong joinTime if having preroll and /init
 - Fix type in Constants.java

## [6.0.8-beta2] - 2017-11-23
### Added
 - Ads after stop

## [6.0.8-beta] - 2017-11-22
### Added
 - Autostart option

## [6.0.7] - 2017-11-15
### Fixed
 - Playhead on ad events
### Removed
 - errorLevel fatal

## [6.0.6] - 2017-11-14
### Fixed
 - Add null check for adInit

## [6.0.5] - 2017-11-08
### Added
 - pauseDuration on stop event
 - DeviceInfo class
### Fixed
 - AdDuration is no 0 on adinit and adstart
 - AdNumber not incrementing
 - If no activity passed but autobackground option enabled display error log message

## [6.0.4] - 2017-10-10
### Added
 - AdInit method

## [6.0.3] - 2017-10-02
### Added
 - Null check when removing activity callbacks
 - AutoDetectBackground option

## [6.0.2] - 2017-09-29
### Added
 - Null check when removing activity callbacks
 - New Ad events, adSkip, adClick
 - Remove unnecesary callback
 - Offline events
### Removed
 - GenericAdsAdapter

## [6.0.1] - 2017-08-25
### Fixed
 - Join time calculation

## [6.0.0] - 2017-07-18
### Fixed
 - First release
 
